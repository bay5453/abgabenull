---
title: "Demo"
author: "Vy"
date: "2022-12-17"
output: 
  html_document:
    keep_md: true
    toc: true
    toc_float: true
    number_sections: true
  pdf_document:
    toc: yes
---

```r
knitr::opts_chunk$set(echo = TRUE)
```

# Code Chunks 

```r
vektor_a <- c(1,2,3)
vektor_b <- c(4,5,6)
```


```
## [1]  4 10 18
```

4, 10, 18
10.6666667

# Test
**zum Beispiel**

* Liebling
+ Kaffee
+ Tee

$$ 2+2 = 4 $$



# R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:


```r
summary(cars)
```

```
##      speed           dist       
##  Min.   : 4.0   Min.   :  2.00  
##  1st Qu.:12.0   1st Qu.: 26.00  
##  Median :15.0   Median : 36.00  
##  Mean   :15.4   Mean   : 42.98  
##  3rd Qu.:19.0   3rd Qu.: 56.00  
##  Max.   :25.0   Max.   :120.00
```

# Including Plots

You can also embed plots, for example:

![](demo_files/figure-html/pressure-1.png)<!-- -->

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
